import pytest

from app import create_app

# Create the Flask application instance
app = create_app()


@pytest.fixture
def client():
    # Create an instance of the application
    app = create_app()
    # Enable testing mode
    app.config['TESTING'] = True
    # Create the test client
    client = app.test_client()
    # Provide the client to the test functions
    yield client


# Define test functions
def test_login(client):
    response = client.post('/', data={'username': 'admin', 'password': '12345'})
    assert response.status_code == 302
    assert b'index' in response.headers['Location'].encode('utf-8')  # Encode to bytes if necessary


def test_move_endpoint(client):
    # Test making a move
    state = {
        'difficulty': 1,  # Medium difficulty
        'board': [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
        'player': 1,
        'computer': 2
    }
    response = client.post('/move', json=state)
    assert response.status_code == 200

    # Check response contains computerMove key
    response_data = response.get_json()
    assert 'computerMove' in response_data
