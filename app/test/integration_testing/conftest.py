import pytest
from app import get_connection, create_app


# Define the test database connection fixture
@pytest.fixture(scope='function')
def test_db_connection():
    """Establish a connection to the test database.

    This fixture sets up a connection to the test database before each test
    and closes the connection after the test completes.

    Yields:
        Connection object: A connection to the test database.
    """
    try:
        # Establish a connection to the test database
        conn = get_connection(test=True)
        yield conn
    finally:
        # Cleanup after test
        if conn:
            conn.close()


@pytest.fixture
def app():
    #return create_app()
    # Set up your app as needed for testing
    app = create_app()
    app.config['TESTING'] = True  # Configure app for testing
    yield app
    # Clean up after tests if needed