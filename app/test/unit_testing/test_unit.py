import app.logic


def test_easy_move():
    game = app.logic.Easy()
    game.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    game.player = 1
    game.computer = 2
    move = game.calculate_move()
    assert move is not None  # Check that a move is made


def test_medium_move():
    game = app.logic.Medium()
    game.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    game.player = 1
    game.computer = 2
    move = game.calculate_move()
    assert move is not None


def test_hard_move():
    game = app.logic.Hard()
    game.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    game.player = 1
    game.computer = 2
    move = game.calculate_move()
    assert move is not None
