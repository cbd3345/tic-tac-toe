import os
from pathlib import Path

from dotenv import load_dotenv
from flask import Flask
from google.cloud.sql.connector import Connector, IPTypes


def create_app():
    app = Flask(__name__)
    from .routes import register_routes
    register_routes(app)
    print("Registered routes:", app.url_map)
    return app


app_directory = Path(__file__).resolve().parents[0]
app = Flask(__name__)
load_dotenv(str(app_directory) + "/.env")
# load_dotenv(".env")

# Set your service account credentials file
#os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = str(app_directory) + "/speedy-center-key.json"

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "$GCLOUD_SERVICE_KEY"

#INSTANCE_CONNECTION_NAME = "speedy-center-420103:northamerica-northeast2:sampletictactoe"

# Database details
#DB_USER = "root"
#DB_PASSWORD = "12345"
#DB_NAME = "tictactoedb"

# Cloud SQL instance connection name

INSTANCE_CONNECTION_NAME = os.getenv("INSTANCE_CONNECTION_NAME")

# Database details
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_NAME = os.getenv("DB_NAME")

print("INSTANCE_CONNECTION_NAME : ", os.getenv("INSTANCE_CONNECTION_NAME"))
print("DB_USER : ", os.getenv("DB_USER"))
print("DB_PASSWORD : ", os.getenv("DB_PASSWORD"))
print("DB_NAME : ", os.getenv("DB_NAME"))
# Initialize Connector
connector = Connector()


def get_connection():
    conn = connector.connect(
        INSTANCE_CONNECTION_NAME,
        "pymysql",
        user=DB_USER,
        password=DB_PASSWORD,
        db=DB_NAME,
        ip_type=IPTypes.PUBLIC,  # Use PRIVATE if using a private IP
    )
    return conn


from app import routes

routes.register_routes(app)
