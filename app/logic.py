import random
from copy import deepcopy


# Basic Helper functions to track the board and calculate computer moves
class Game:
    def __init__(self, player='o', computer='x'):
        self.player = player
        self.computer = computer
        self.board = board = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']

    # Returns a copy of the board to experiment on
    def board_copy(self):
        return deepcopy(self.board)

    # Returns true if a particular position in the board is empty
    def position_empty(self, pos):
        if (self.board[pos] == ' '):
            return True
        else:
            return False

    # Returns a list of possible moves
    def get_possible_moves(self):
        moves = []
        for i in range(len(self.board)):
            if (self.position_empty(i)):
                moves.append(i)
        return moves

    # Returns true if the board is full
    def is_board_full(self):
        for i in range(0, len(self.board)):
            if self.board[i] == ' ':
                return False
        return True

    # Returns the winning combination or returns False
    def has_won(self, b, mark):
        # Horizontal win
        if b[0] == b[1] and b[0] == b[2] and b[0] == mark:
            return [[0, 1, 2]]
        elif b[3] == b[4] and b[3] == b[5] and b[3] == mark:
            return [[3, 4, 5]]
        elif b[6] == b[7] and b[6] == b[8] and b[6] == mark:
            return [[6, 7, 8]]

            # Vertical Win
        elif b[0] == b[3] and b[0] == b[6] and b[0] == mark:
            return [[0, 3, 6]]
        elif b[1] == b[4] and b[1] == b[7] and b[1] == mark:
            return [[1, 4, 7]]
        elif b[2] == b[5] and b[2] == b[8] and b[2] == mark:
            return [[2, 5, 8]]

            # Diagonal Win
        elif b[0] == b[4] and b[0] == b[8] and b[0] == mark:
            return [[0, 4, 8]]
        elif b[2] == b[4] and b[2] == b[6] and b[2] == mark:
            return [[2, 4, 6]]
        else:
            return False


# Easy bot
# Picks winning moves.
# Else, it makes random moves.
class Easy(Game):

    def calculate_move(self):

        moves = self.get_possible_moves()
        if (len(moves) == 0):
            return -1

        # Check for winning move
        for move in moves:
            board_copy = self.board_copy()
            board_copy[move] = self.computer
            if (self.has_won(board_copy, self.computer)):
                return move

        # Return a random move
        return random.choice(moves)


# Medium bot
#   Picks winning moves.
#   Blocks opponent's win opportunities.
#   Else makes random moves
class Medium(Game):
    def calculate_move(self):

        moves = self.get_possible_moves()
        if (len(moves) == 0):
            return -1

        # Check for winning move
        for move in moves:
            board_copy = self.board_copy()
            board_copy[move] = self.computer
            if (self.has_won(board_copy, self.computer)):
                return move

        # Check if opponent can win next turn and block that move
        for move in moves:
            board_copy = self.board_copy()
            board_copy[move] = self.player
            if (self.has_won(board_copy, self.player)):
                return move

        # Return a random move
        return random.choice(moves)


# Hard bot
#   Picks winning moves.
#   Blocks opponent's win opportunities.
#   Else, makes selected moves based on game theory.
class Hard(Game):

    def calculate_move(self):

        moves = self.get_possible_moves()
        if (len(moves) == 0):
            return -1

        # Check for winning move
        for move in moves:
            board_copy = self.board_copy()
            board_copy[move] = self.computer
            if (self.has_won(board_copy, self.computer)):
                return move

        # Check if opponent can win next turn and block that move
        for move in moves:
            board_copy = self.board_copy()
            board_copy[move] = self.player
            if (self.has_won(board_copy, self.player)):
                return move

        corners = [0, 2, 6, 8]
        middle = [4]
        other = [1, 3, 5, 7]

        # Choose corners if possible
        moveset = list(set(corners).intersection(set(moves)))
        if (len(moveset) != 0):
            return random.choice(moveset)

        # Choose middle cell if corners are taken
        moveset = list(set(middle).intersection(set(moves)))
        if (len(moveset) != 0):
            return random.choice(moveset)

        # Make a choice from one of the remaining cells.
        moveset = list(set(other).intersection(set(moves)))
        return random.choice(moveset)
