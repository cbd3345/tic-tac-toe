from flask import render_template, request, redirect, url_for, session, jsonify

from app import get_connection
from app.logic import Easy, Medium, Hard


def register_routes(app):
    # Main game page
    @app.route('/index')
    def index():
        return render_template('index.html')

    # Login route
    @app.route('/', methods=['GET', 'POST'])
    def login():
        if request.method == 'POST':
            username = request.form['username']
            password = request.form['password']

            # Establish a database connection
            conn = get_connection()

            try:
                # Create a cursor object
                cursor = conn.cursor()

                # Execute the query to find the user
                query = "SELECT * FROM users WHERE " \
                        "username = %s AND password = %s"
                cursor.execute(query, (username, password))

                # Fetch one record from the result set
                user = cursor.fetchone()

                # Check if a user record was found
                if user:
                    # Here you might want to redirect to the main game page
                    return redirect(url_for('index'))
                else:
                    # Invalid credentials
                    # render login page again with a message
                    return render_template('login.html', message='Invalid username or password')

            finally:
                # Close the cursor and connection
                cursor.close()
                conn.close()

        return render_template('login.html')

    @app.route('/logout', methods=['GET', 'POST'])
    def logout():
        session.pop('username', None)
        return redirect(url_for('login.html'))

    # Endpoint for making a move in the game
    @app.route('/move', methods=['POST'])
    def move():
        state = request.get_json()

        difficulty = state.get('difficulty')

        # Choose game mode based on difficulty
        if difficulty == 0:
            game = Easy()
        elif difficulty == 1:
            game = Medium()
        else:
            game = Hard()

        game.board = state.get('board')
        game.player = state.get('player')
        game.computer = state.get('computer')

        # Calculate computer move
        move = game.calculate_move()

        # Response sent with calculated move
        return jsonify(computerMove=move)

    @app.route('/<path:path>')
    def catch_all(path):
        return f"The requested URL {path} was not found on the server."

