FROM python:3.11-alpine


WORKDIR /

# RUN apt-get update
COPY . .

RUN pip install --no-cache-dir --upgrade pip
#RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir -r requirements.txt -v



EXPOSE 7001
CMD ["python", "-u", "main.py"]